import os
import flask
import numpy


app = flask.Flask(__name__)


@app.route("/")
def index():
    return 'QA service main page'

@app.get("/health", status_code=status.HTTP_200_OK)
def health():
    return flask.jsonify({"status": "fine"})

app.run(host='0.0.0.0', port=5000)